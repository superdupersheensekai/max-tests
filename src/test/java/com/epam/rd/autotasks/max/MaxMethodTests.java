package com.epam.rd.autotasks.max;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Duration;
import java.util.Arrays;
import java.util.OptionalInt;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class MaxMethodTests {
    @Test
    @DisplayName("For a null returns empty")
    public void testReturnsEmptyForNull() {
        int[] values = null;
        OptionalInt result = MaxMethod.max(values);
        assertEquals(OptionalInt.empty(), result);
    }

    @Test
    @DisplayName("For an empty array return empty")
    void testReturnsEmptyForEmptyArray() {
        int[] values = new int[0];
        OptionalInt result = MaxMethod.max(values);
        assertEquals(OptionalInt.empty(), result);
    }

    @Test
    @DisplayName("For an array of length 1 returns its only value")
    void testReturnArraysSingleValue() {
        int[] values = new int[]{10};
        OptionalInt result = MaxMethod.max(values);
        assertNotNull(result);
        assertTrue(result.isPresent());
        assertEquals(values[0], result.getAsInt());
    }

    @Test
    @DisplayName("For an array of negative ints returns correct max")
    void testReturnMaxForNegativeValues() {
        int[] values = new int[]{-4, -3, -3, -10, -2, -145};
        OptionalInt result = MaxMethod.max(values);
        assertNotNull(result);
        assertTrue(result.isPresent());
        assertEquals(-2, result.getAsInt());
    }

    @Test
    @DisplayName("For an array containing MAX_VALUE return the value")
    void testReturnIntegerMaxIfArrayContainsIt() {
        int[] values = new int[]{-10, 3, 255528, 0, Integer.MAX_VALUE, 100, 0};
        OptionalInt result = MaxMethod.max(values);
        assertNotNull(result);
        assertTrue(result.isPresent());
        assertEquals(Integer.MAX_VALUE, result.getAsInt());
    }

    @Test
    @DisplayName("For an array containing value near to MAX_VALUE return the value")
    void testReturnsValueNearToIntegerMaxIfArrayContainsIt() {
        int diff = 27;
        int toFind = Integer.MAX_VALUE - diff;
        int[] values = new int[]{8332, 0, -123, 44, 23, 924823, toFind};
        OptionalInt result = MaxMethod.max(values);
        assertNotNull(result);
        assertTrue(result.isPresent());
        assertEquals(toFind, result.getAsInt());
    }

    @Test
    void testReturnMaxValueForSampleArray() {
        int[] values = new int[] {-574, -484, 444, -217, 978, -370, 269, -9, 189};
        OptionalInt result = MaxMethod.max(values);
        assertNotNull(result);
        assertTrue(result.isPresent());
        assertEquals(978, result.getAsInt());
    }

    @Test
    void testReturnMaxValueForSampleArray2() {
        int[] values = new int[] {-794,-206,-500,-410,-850, -249,-968,-536, -61, -65,-866};
        OptionalInt result = MaxMethod.max(values);
        assertNotNull(result);
        assertTrue(result.isPresent());
        assertEquals(-61, result.getAsInt());
    }

    @Test
    void testReturnMaxValueForSampleArray3() {
        int[] values = new int[] {806,  19, 140, 392,  36, 528, 351,  29, 786, 646, 656, 542, 890, 424};
        OptionalInt result = MaxMethod.max(values);
        assertNotNull(result);
        assertTrue(result.isPresent());
        assertEquals(890, result.getAsInt());
    }

    @ParameterizedTest
    @MethodSource("makeValues")
    @DisplayName("For an arrays of different lengths returns their max values")
    void testReturnsMaxValue(int[] values) {
        int max = IntStream.of(values).reduce(Integer.MIN_VALUE, Math::max);
        OptionalInt result = MaxMethod.max(values);
        assertNotNull(result);
        assertTrue(result.isPresent());
        assertEquals(max, result.getAsInt());
    }

    @ParameterizedTest
    @MethodSource("makeBigSizeValues")
    @DisplayName("Doesn't process arrays too slow")
    void testWorksNotTooSlow(int[] values) {
        int millisTimeout = 100;
        assertTimeoutPreemptively(Duration.ofMillis(millisTimeout),
                () -> MaxMethod.max(values));
    }

    @ParameterizedTest
    @MethodSource("makeBigSizeValues")
    @DisplayName("Doesn't change received array")
    void testDoesntChangeReceivedArray(int[] values) {
        int[] copy = Arrays.copyOf(values, values.length);
        MaxMethod.max(copy);
        assertArrayEquals(values, copy);
    }

    static Stream<int[]> makeValues() {
        int maxLen = 50000000;
        Random rand = new Random();
        Stream.Builder<int[]> builder = Stream.builder();
        for (int len = 1; len <= maxLen; len *= 2) {
            int[] values = new int[len];
            for (int i = 0; i < len; ++i) {
                values[i] = rand.nextInt();
            }
            builder.add(values);
        }

        return builder.build();
    }

    static Stream<int[]> makeBigSizeValues() {
        int len = 30000000;
        Random rand = new Random();
        int[] values = new int[len];
        for (int i = 0; i < len; ++i) {
            values[i] = rand.nextInt();
        }
        return Stream.of(values);
    }
}
